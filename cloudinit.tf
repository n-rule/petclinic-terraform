data "cloudinit_config" "cloudinit-app" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content = templatefile("scripts/app-init.cfg", {
      REGION = var.AWS_REGION
    })
  }

  part {
    content_type = "text/x-shellscript"
    content = templatefile("scripts/app-script.sh", {
      PETCLINIC_LOCATION = var.PETCLINIC_LOCATION,
      MYSQL_USER         = aws_db_instance.petclinic-db.username,
      MYSQL_PASS         = aws_db_instance.petclinic-db.password,
      MYSQL_URL          = "jdbc:mysql://${aws_db_instance.petclinic-db.endpoint}/petclinic",
      #DB_IP              = aws_instance.petclinic-db.private_ip
    })
  }
}

