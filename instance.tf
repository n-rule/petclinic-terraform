data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "petclinic-app" {

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.micro"
  iam_instance_profile   = aws_iam_instance_profile.petclinic-profile.name
  subnet_id              = aws_subnet.petclinic-public-1.id
  vpc_security_group_ids = [aws_security_group.petclinic-app-sg.id]
  user_data              = data.cloudinit_config.cloudinit-app.rendered

  depends_on = [
    aws_db_instance.petclinic-db
  ]

  tags = {
    Name = "petclinic-app"
  }
}



