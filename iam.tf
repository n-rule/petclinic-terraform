# CREATE THE AWS IAM ROLE

resource "aws_iam_role" "petclinic-demigod" {
  name = "petclinic-demigod"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

# ATTACH THE AWS EXISTING POLICY TO THE IAM ROLE


data "aws_iam_policy" "AmazonSSMManagedInstanceCore" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "AmazonSSMManagedInstanceCore-attach" {
  role       = aws_iam_role.petclinic-demigod.name
  policy_arn = data.aws_iam_policy.AmazonSSMManagedInstanceCore.arn
}

# CREATE AWS IAM INSTANCE PROFILE AND ATTACH ROLE TO IT

resource "aws_iam_instance_profile" "petclinic-profile" {
  name = "petclinic-profile"
  role = aws_iam_role.petclinic-demigod.name
}



