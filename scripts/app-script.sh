#!/bin/bash

wget ${PETCLINIC_LOCATION} -P /tmp

tee /etc/systemd/petclinic-env.conf >/dev/null <<EOF
MYSQL_USER=${MYSQL_USER}
MYSQL_PASS=${MYSQL_PASS}
MYSQL_URL=${MYSQL_URL}
EOF

cat <<EOF > /etc/systemd/system/petclinic.service
[Unit]
Description= PetClinic MySQL

[Service]
EnvironmentFile=/etc/systemd/petclinic-env.conf
ExecStart=java -Dspring.profiles.active=mysql -jar /tmp/petclinic.jar

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl start petclinic.service 

